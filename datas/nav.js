var navs = [
{
	"title": "森特后台",
	"icon": "fa-cubes",
	"spread": true,
	"children": [{
		"title": "用户管理",
		"icon": "&#xe641;",
		"href": "user.html"
	}, {
		"title": "角色管理",
		"icon": "&#xe63c;",
		"href": "usermanage.html"
	}, {
		"title": "菜单管理",
		"icon": "&#xe63c;",
		"href": "menuManage.html"
	}, {
		"title": "部门管理",
		"icon": "&#xe609;",
		"href": "department.html"
	}]
},
{
	"title": "森特后台",
	"icon": "fa-cubes",
	"spread": false,
	"children": [{
		"title": "用户管理",
		"icon": "&#xe641;",
		"href": "user.html"
	}, {
		"title": "表单",
		"icon": "&#xe63c;",
		"href": "form.html"
	}, {
		"title": "表格",
		"icon": "&#xe63c;",
		"href": "table.html"
	}, {
		"title": "导航",
		"icon": "&#xe609;",
		"href": "nav.html"
	}, {
		"title": "辅助性元素",
		"icon": "&#xe60c;",
		"href": "auxiliar.html"
	}]
}
];